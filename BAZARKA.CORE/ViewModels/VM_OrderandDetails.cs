﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BAZARKA.CORE.Models;
using System.ComponentModel.DataAnnotations;

namespace BAZARKA.CORE.ViewModels
{
    public class VM_OrderandDetails
    {
        [Key]
        public int ID { get; set; }
        public Order Order { get; set; }
        public List<OrderDetail> OrderDetails { get; set; }
    }
}
