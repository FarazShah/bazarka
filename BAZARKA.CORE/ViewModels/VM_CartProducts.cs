﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAZARKA.CORE.ViewModels
{
    public class VM_CartProducts
    {
        public int quantity { get; set; }
        public string sku { get; set; }
    }
}
