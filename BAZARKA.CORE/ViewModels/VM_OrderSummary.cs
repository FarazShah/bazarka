﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAZARKA.CORE.ViewModels
{
    public static class VM_OrderSummary
    {
        public static double MerchandiseSubTotal { get; set; }
        public static double Shipping { get; set; }
        public static double Tax { get; set; }
        public static int TaxPercentApplied { get; set; }
        public static double EstimatedTotal { get; set; }
    }
}
