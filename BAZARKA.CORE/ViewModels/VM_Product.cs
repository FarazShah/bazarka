﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAZARKA.CORE.ViewModels
{

    public class VM_Product
    {
        public string ProductSKU { get; set; }
        public string ProductName { get; set; }
        public string ShortDesc { get; set; }
        public string ProductThumbnail { get; set; }
        public int UnitPrice { get; set; }
    }
}
