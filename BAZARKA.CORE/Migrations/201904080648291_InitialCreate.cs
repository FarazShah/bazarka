namespace BAZARKA.CORE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BannerInformations",
                c => new
                    {
                        BannerID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        StartDateTime = c.DateTime(nullable: false),
                        EndDateTime = c.DateTime(nullable: false),
                        BannerInterval = c.Int(nullable: false),
                        Clicked = c.Int(nullable: false),
                        active = c.Boolean(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        CreatedBy = c.DateTime(nullable: false),
                        UpdatedBy = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.BannerID);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        CatID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ParentID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CatID);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ProductID = c.Int(nullable: false, identity: true),
                        SKU = c.String(),
                        VendorProductID = c.Int(nullable: false),
                        ProductName = c.String(),
                        ProductBrand = c.String(),
                        ProductThumbnail = c.String(),
                        QTYPerUnit = c.Int(nullable: false),
                        UnitSize = c.Int(nullable: false),
                        UnitPrice = c.Int(nullable: false),
                        MSRP = c.Double(nullable: false),
                        AvailableSize = c.String(),
                        AvailableColor = c.Int(nullable: false),
                        SizeID = c.Int(nullable: false),
                        ColorID = c.Int(nullable: false),
                        Discount = c.Double(nullable: false),
                        UnitWeight = c.Double(nullable: false),
                        UnitsInStock = c.Int(nullable: false),
                        UnitOnOrder = c.Int(nullable: false),
                        ReOrderLevel = c.Int(nullable: false),
                        Availability = c.Boolean(nullable: false),
                        DiscountAvailable = c.Boolean(nullable: false),
                        weightage = c.Int(nullable: false),
                        SearchWeight = c.String(),
                        Note = c.String(),
                        Active = c.Boolean(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        CreatedBy = c.DateTime(nullable: false),
                        UpdatedBy = c.DateTime(nullable: false),
                        VendorID = c.Int(nullable: false),
                        CatID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProductID)
                .ForeignKey("dbo.Categories", t => t.CatID, cascadeDelete: true)
                .ForeignKey("dbo.Vendors", t => t.VendorID, cascadeDelete: true)
                .Index(t => t.VendorID)
                .Index(t => t.CatID);
            
            CreateTable(
                "dbo.ProductDetails",
                c => new
                    {
                        ProductDetailID = c.Int(nullable: false),
                        ShortDesc = c.String(),
                        LongDesc = c.String(),
                        Specification = c.String(),
                        WhatInTheBox = c.String(),
                        RefundPolicy = c.String(),
                        ProductWarrantyShort = c.String(),
                        ProductWarrantyLong = c.String(),
                        image1 = c.String(),
                        image2 = c.String(),
                        image3 = c.String(),
                        image4 = c.String(),
                        FreeShipping = c.String(),
                        VideoURL = c.String(),
                        MetaTitle = c.String(),
                        MetaKeywords = c.String(),
                        MetaDescription = c.String(),
                        WeightGrams = c.Int(nullable: false),
                        ProductKeywords = c.String(),
                        DeliveryTime = c.String(),
                        YoutubeLink = c.String(),
                        SKUCity = c.String(),
                        UPC = c.String(),
                        HelpText = c.String(),
                    })
                .PrimaryKey(t => t.ProductDetailID)
                .ForeignKey("dbo.Products", t => t.ProductDetailID)
                .Index(t => t.ProductDetailID);
            
            CreateTable(
                "dbo.Reviews",
                c => new
                    {
                        ReviewID = c.Int(nullable: false, identity: true),
                        ProductSKU = c.String(),
                        Title = c.String(),
                        Desciption = c.String(),
                        Star = c.Int(nullable: false),
                        HelpFull = c.Int(nullable: false),
                        NotHelpFull = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        updateDate = c.DateTime(nullable: false),
                        UpdateBy = c.DateTime(nullable: false),
                        CustomerID = c.String(),
                        ProductID = c.Int(nullable: false),
                        Customer_CustomerID = c.Int(),
                    })
                .PrimaryKey(t => t.ReviewID)
                .ForeignKey("dbo.CustomerProfiles", t => t.Customer_CustomerID)
                .ForeignKey("dbo.Products", t => t.ProductID, cascadeDelete: true)
                .Index(t => t.ProductID)
                .Index(t => t.Customer_CustomerID);
            
            CreateTable(
                "dbo.CustomerProfiles",
                c => new
                    {
                        CustomerID = c.Int(nullable: false, identity: true),
                        FullName = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        password = c.String(nullable: false),
                        ContactNum = c.String(nullable: false),
                        Birthdate = c.DateTime(nullable: false),
                        Active = c.Boolean(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                    })
                .PrimaryKey(t => t.CustomerID);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        OrderID = c.Int(nullable: false, identity: true),
                        OrderDate = c.DateTime(nullable: false),
                        RequiredDate = c.DateTime(nullable: false),
                        ShipDate = c.DateTime(nullable: false),
                        ShipperiD = c.Int(nullable: false),
                        SalesTax = c.Double(nullable: false),
                        TransactStatus = c.String(),
                        ErrMsg = c.String(),
                        ErrDesc = c.String(),
                        PaymentDate = c.DateTime(nullable: false),
                        FullFiled = c.Boolean(nullable: false),
                        Active = c.Boolean(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        CreatedBy = c.DateTime(nullable: false),
                        UpdateBy = c.DateTime(nullable: false),
                        CustomerID = c.Int(nullable: false),
                        PaymentID = c.Int(nullable: false),
                        ProcessID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrderID)
                .ForeignKey("dbo.CustomerProfiles", t => t.CustomerID, cascadeDelete: true)
                .ForeignKey("dbo.Payments", t => t.PaymentID, cascadeDelete: true)
                .ForeignKey("dbo.ProcessFlows", t => t.ProcessID, cascadeDelete: true)
                .Index(t => t.CustomerID)
                .Index(t => t.PaymentID)
                .Index(t => t.ProcessID);
            
            CreateTable(
                "dbo.OrderDetails",
                c => new
                    {
                        OrderDetailID = c.Int(nullable: false, identity: true),
                        ProductSKU = c.String(),
                        Price = c.Double(nullable: false),
                        Quantity = c.Int(nullable: false),
                        Discount = c.Double(nullable: false),
                        Total = c.Double(nullable: false),
                        Size = c.String(),
                        Color = c.String(),
                        FulFilled = c.Boolean(nullable: false),
                        ShipDate = c.DateTime(nullable: false),
                        ShipperID = c.Int(nullable: false),
                        ShippingCharges = c.Double(nullable: false),
                        SalesTax = c.Double(nullable: false),
                        Rejected = c.Boolean(nullable: false),
                        RejectionDetail = c.String(),
                        OrderID = c.Int(nullable: false),
                        ProductID = c.Int(nullable: false),
                        PromoID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrderDetailID)
                .ForeignKey("dbo.Orders", t => t.OrderID, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductID, cascadeDelete: true)
                .ForeignKey("dbo.Promotions", t => t.PromoID, cascadeDelete: true)
                .Index(t => t.OrderID)
                .Index(t => t.ProductID)
                .Index(t => t.PromoID);
            
            CreateTable(
                "dbo.Promotions",
                c => new
                    {
                        PromoID = c.Int(nullable: false, identity: true),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Price = c.Double(nullable: false),
                        MSRP = c.Double(nullable: false),
                        Quantity = c.Int(nullable: false),
                        PromoPaymentMethod = c.String(),
                        PromoCyclePerCustomer = c.Int(nullable: false),
                        Type = c.String(),
                        BIN = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        CreatedBy = c.DateTime(nullable: false),
                        UpdatedBy = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.PromoID);
            
            CreateTable(
                "dbo.Payments",
                c => new
                    {
                        PaymentID = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.PaymentID);
            
            CreateTable(
                "dbo.ProcessFlows",
                c => new
                    {
                        ProcessID = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.ProcessID);
            
            CreateTable(
                "dbo.Vendors",
                c => new
                    {
                        VendorID = c.Int(nullable: false, identity: true),
                        CompanyName = c.Int(nullable: false),
                        ContactFName = c.String(),
                        ContactLName = c.String(),
                        ContactTitle = c.String(),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        City = c.String(),
                        State = c.String(),
                        PostalCode = c.String(),
                        Country = c.String(),
                        PhoneNo = c.String(),
                        Email = c.String(),
                        Website = c.String(),
                        PaymentMethod = c.String(),
                        DiscountType = c.String(),
                        DiscountRate = c.Double(nullable: false),
                        TypeGoods = c.String(),
                        DiscountAvailable = c.Boolean(nullable: false),
                        CurrentOrder = c.Int(nullable: false),
                        ProdInfoPageURL = c.String(),
                        Logo = c.String(),
                        VendorRanking = c.Single(nullable: false),
                        Note = c.String(),
                        Active = c.Boolean(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        CreatedBy = c.DateTime(nullable: false),
                        UpdateBy = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.VendorID);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.SearchHistories",
                c => new
                    {
                        SearchID = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        Date = c.DateTime(nullable: false),
                        CustomerID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SearchID)
                .ForeignKey("dbo.CustomerProfiles", t => t.CustomerID, cascadeDelete: true)
                .Index(t => t.CustomerID);
            
            CreateTable(
                "dbo.Subscribers",
                c => new
                    {
                        SubID = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                        Active = c.Boolean(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.SubID);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        DateOfBirth = c.DateTime(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SearchHistories", "CustomerID", "dbo.CustomerProfiles");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Products", "VendorID", "dbo.Vendors");
            DropForeignKey("dbo.Reviews", "ProductID", "dbo.Products");
            DropForeignKey("dbo.Reviews", "Customer_CustomerID", "dbo.CustomerProfiles");
            DropForeignKey("dbo.Orders", "ProcessID", "dbo.ProcessFlows");
            DropForeignKey("dbo.Orders", "PaymentID", "dbo.Payments");
            DropForeignKey("dbo.OrderDetails", "PromoID", "dbo.Promotions");
            DropForeignKey("dbo.OrderDetails", "ProductID", "dbo.Products");
            DropForeignKey("dbo.OrderDetails", "OrderID", "dbo.Orders");
            DropForeignKey("dbo.Orders", "CustomerID", "dbo.CustomerProfiles");
            DropForeignKey("dbo.ProductDetails", "ProductDetailID", "dbo.Products");
            DropForeignKey("dbo.Products", "CatID", "dbo.Categories");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.SearchHistories", new[] { "CustomerID" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.OrderDetails", new[] { "PromoID" });
            DropIndex("dbo.OrderDetails", new[] { "ProductID" });
            DropIndex("dbo.OrderDetails", new[] { "OrderID" });
            DropIndex("dbo.Orders", new[] { "ProcessID" });
            DropIndex("dbo.Orders", new[] { "PaymentID" });
            DropIndex("dbo.Orders", new[] { "CustomerID" });
            DropIndex("dbo.Reviews", new[] { "Customer_CustomerID" });
            DropIndex("dbo.Reviews", new[] { "ProductID" });
            DropIndex("dbo.ProductDetails", new[] { "ProductDetailID" });
            DropIndex("dbo.Products", new[] { "CatID" });
            DropIndex("dbo.Products", new[] { "VendorID" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Subscribers");
            DropTable("dbo.SearchHistories");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Vendors");
            DropTable("dbo.ProcessFlows");
            DropTable("dbo.Payments");
            DropTable("dbo.Promotions");
            DropTable("dbo.OrderDetails");
            DropTable("dbo.Orders");
            DropTable("dbo.CustomerProfiles");
            DropTable("dbo.Reviews");
            DropTable("dbo.ProductDetails");
            DropTable("dbo.Products");
            DropTable("dbo.Categories");
            DropTable("dbo.BannerInformations");
        }
    }
}
