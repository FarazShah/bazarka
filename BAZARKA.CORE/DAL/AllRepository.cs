﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
//using BAZARKA.CORE.Identity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using BAZARKA.CORE.Identity;
using System;

namespace BAZARKA.CORE.DAL
{
    public class AllRepository<T> : _IRepository<T> where T : class
    {
        private BaseContext Context;
        private IDbSet<T> dbEntity;

        public AllRepository()
        {
            Context = new BaseContext();
            dbEntity = Context.Set<T>();
        } 

        public void DeleteModel(int modelId)
        {
            T model = dbEntity.Find(modelId);
            dbEntity.Remove(model);
        }

        public IEnumerable<T> GetModel()
        {
            return dbEntity.ToList();
        }

        public T GetModelByID(int modelId)
        {
            return dbEntity.Find(modelId);
        }

        public void InsertModel(T model)
        {
            dbEntity.Add(model);
        }

        public int Save()
        {
            int retvalue = 0;
            try
            {
                retvalue = Context.SaveChanges();
            }catch(Exception ex)
            {
                retvalue = 0;
            }
            return retvalue;
        }

        public void UpdateModel(T model)
        {
            Context.Entry(model).State = EntityState.Modified;
        }

        public List<T> ExecuteSp(string sp_name)
        {
            List<T> retvalue = Context.Database.SqlQuery<T>(sp_name).ToList();
            return retvalue;
        }
    }
}
