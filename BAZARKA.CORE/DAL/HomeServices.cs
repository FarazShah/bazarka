﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BAZARKA.CORE.DAL;
using BAZARKA.CORE;
using System.Web;
using BAZARKA.CORE.Models;
using BAZARKA.CORE.Identity;
using BAZARKA.CORE.ViewModels;

namespace BAZARKA.CORE.DAL
{
    public class HomeServices
    {

        AllRepository<Product> ProductRepo;
        AllRepository<OrderDetail> OrderRepo;
        AllRepository<VM_Product> HomeContentRepo;
        public HomeServices()
        {
            ProductRepo = new AllRepository<Product>();
            OrderRepo = new AllRepository<OrderDetail>();
            HomeContentRepo = new AllRepository<VM_Product>();
        }

        public List<Product> GetJustArrived()
        {
            List<Product> oProducts = null;
            try
            {
                var retList = (from c in ProductRepo.GetModel().Where(c => c.Active = true)
                    .Select(c => new { c.ProductName, c.SKU, c.ProductThumbnail, c.ProductDetail.ShortDesc, c.UnitPrice, c.CreateDate })
                    .Distinct()
                    .Take(18)
                    .OrderBy(c => c.CreateDate)
                               select c).ToList();
                if (retList != null)
                {
                    oProducts = new List<Product>();
                    Product product = null;
                    foreach (var obj in retList)
                    {
                        product = new Product();
                        product.ProductDetail = new ProductDetail();
                        product.ProductName = obj.ProductName;
                        product.ProductThumbnail = obj.ProductThumbnail;
                        product.ProductDetail.ShortDesc = obj.ShortDesc;
                        product.SKU = obj.SKU;
                        product.CreateDate = obj.CreateDate;
                        product.UnitPrice = obj.UnitPrice;
                        oProducts.Add(product);
                    }
                }
            }
            catch (Exception ex)
            {
                oProducts = null;
            }
            return oProducts;
        }

        public List<Product> GetOffersPics()
        {
            List<Product> oProducts = null;
            try
            {
                //var retList = (from c in ProductRepo.GetModel().Where(c => c.Active = true)
                //    .Select(c => new { c.ProductName, c.ProductThumbnail, c.ProductDetail.ShortDesc, c.CreateDate })
                //    .Distinct()
                //    .OrderBy(c => c.CreateDate)
                //               select c).ToList();
                //if (retList != null)
                //{
                //    Product product = null;
                //    foreach (var obj in retList)
                //    {
                //        product = new Product();
                //        product.ProductName = obj.ProductName;
                //        product.ProductThumbnail = obj.ProductThumbnail;
                //        product.ProductDetail.ShortDesc = obj.ShortDesc;
                //        product.CreateDate = obj.CreateDate;
                //        oProducts.Add(product);
                //    }
                //}
            }
            catch (Exception ex)
            {
                oProducts = null;
            }
            return oProducts;
        }

        public List<Product> GetEditorsPick()
        {
            List<Product> oProducts = null;
            try
            {
                var retList = (from c in ProductRepo.GetModel().Where(c => c.Active = true)
                    .Select(c => new { c.ProductName,c.SKU, c.ProductThumbnail, c.ProductDetail.ShortDesc, c.CreateDate })
                               select c).ToList();
                if (retList != null)
                {
                    oProducts = new List<Product>();
                    Product product = null;
                    foreach (var obj in retList)
                    {
                        product = new Product();
                        product.ProductDetail = new ProductDetail();
                        product.ProductName = obj.ProductName;
                        product.ProductThumbnail = obj.ProductThumbnail;
                        product.ProductDetail.ShortDesc = obj.ShortDesc;
                        product.SKU = obj.SKU;
                        product.CreateDate = obj.CreateDate;
                        oProducts.Add(product);
                    }
                }
            }
            catch (Exception ex)
            {
                oProducts = null;
            }   
            return oProducts;
        }

        public List<VM_Product> GetBestSellers()
        {
            List<VM_Product> result = HomeContentRepo.ExecuteSp("Sp_GetBestSellerItems");
            return result;
        }

        public List<Product> GetRecommendedProducts()
        {
            List<Product> oProducts = null;
            try
            {
                var retList = (from c in ProductRepo.GetModel().Where(c => c.Active = true)
                    .Select(c => new { c.ProductName,c.SKU, c.ProductThumbnail, c.ProductDetail.ShortDesc, c.CreateDate })
                    .Distinct()
                    .OrderBy(c => c.CreateDate)
                               select c).ToList();
                if (retList != null)
                {
                    oProducts = new List<Product>();
                    Product product = null;
                    foreach (var obj in retList)
                    {
                        product = new Product();
                        product.ProductName = obj.ProductName;
                        product.ProductThumbnail = obj.ProductThumbnail;
                        product.ProductDetail.ShortDesc = obj.ShortDesc;
                        product.SKU = obj.SKU;
                        product.CreateDate = obj.CreateDate;
                        oProducts.Add(product);
                    }
                }
            }
            catch (Exception ex)
            {
                oProducts = null;
            }
            return oProducts;
        }

        public List<Product> GetSpotItShopIt()
        {
            List<Product> oProducts = null;
            try
            {
                //var retList = (from c in ProductRepo.GetModel().Where(c => c.Active = true)
                //        .Select(c => new { c.ProductName, c.ProductThumbnail, c.ProductDetail.ShortDesc, c.CreateDate })
                //        .Distinct()
                //        .OrderBy(c => c.CreateDate)
                //                   select c).ToList();
                //    if (retList != null)
                //    {
                //        oProducts = new List<Product>();
                //        Product product = null;
                //        foreach (var obj in retList)
                //        {
                //            product = new Product();
                //            product.ProductName = obj.ProductName;
                //            product.ProductThumbnail = obj.ProductThumbnail;
                //            product.ProductDetail.ShortDesc = obj.ShortDesc;
                //            product.CreateDate = obj.CreateDate;
                //            oProducts.Add(product);
                //        }
                //    }
            }
            catch (Exception ex)
            {
                oProducts = null;
            }
            return oProducts;
        }

    }
}
