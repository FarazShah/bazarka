﻿using System.Collections.Generic;

namespace BAZARKA.CORE.DAL
{
    public interface _IRepository<T> where T : class
    {
        IEnumerable<T> GetModel();
        T GetModelByID(int modelId);
        void InsertModel(T model);
        void DeleteModel(int modelId);
        void UpdateModel(T model);
        int Save();
    }
}
