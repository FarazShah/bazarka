﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BAZARKA.CORE.DAL;
using BAZARKA.CORE.Models;
using System.Web.Services;
using BAZARKA.CORE.Identity;
using System.Data.SqlClient;
using BAZARKA.CORE.ViewModels;

namespace BAZARKA.CORE.DAL
{
    public class ProductServices
    {
        AllRepository<Product> oProductRepo = null;
        public static ProductServices Instance
        {
            get
            {
                if (instance == null) instance = new ProductServices();
                return instance;
            }
        }
        private static ProductServices instance { get; set; }

        private ProductServices()
        {
            oProductRepo = new AllRepository<Product>();
        }

        public List<Product> GetProducts(List<string> ProductSKUs)
        {
            List<Product> oProdList = null;
            try
            {
                oProdList = oProductRepo.GetModel().Where(x => ProductSKUs.Contains(x.SKU)).ToList();
            }
            catch (Exception ex)
            {
                oProdList = null;
            }
            return oProdList;
        }

        public double GetProductUnitPrice(string SKU)
        {
            double retval = 0;
            try
            {
                retval = (from a in oProductRepo.GetModel() where a.SKU == SKU select a.UnitPrice).SingleOrDefault();
            }
            catch (Exception ex)
            {
                retval = 0;
            }
            return retval;
        }

        [WebMethod]
        public List<VM_Product> GetProducts(int pagenumber, int pagesize)
        {
            List<VM_Product> Products = null;
            VM_Product Product = null;
            using (BaseContext ctx = new BaseContext())
            {
                var retvalue = ctx.Database.SqlQuery<VM_Product>("Sp_GetProducts @PageNumber, @PageSize", 
                    new SqlParameter("PageNumber", pagenumber), 
                    new SqlParameter("PageSize", pagesize));
                if(retvalue != null)
                {
                    Products = new List<VM_Product>();
                    foreach(var obj in retvalue)
                    {
                        Product = new VM_Product();
                        Product.ProductName = obj.ProductName;
                        Product.ProductSKU = obj.ProductSKU;
                        Product.ProductThumbnail = obj.ProductThumbnail;
                        Product.ShortDesc = obj.ShortDesc;
                        Product.UnitPrice = obj.UnitPrice;
                        Products.Add(Product);
                    }
                }
            }
            return Products;
        }
    }
}
