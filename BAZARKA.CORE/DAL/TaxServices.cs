﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BAZARKA.CORE.Models;

namespace BAZARKA.CORE.DAL
{
    public class TaxServices
    {
        AllRepository<Taxation> oTaxRepo;
        public TaxServices()
        {
            oTaxRepo = new AllRepository<Taxation>();
        }

        public int GetDefaulTaxPercent()
        {
            int retvalue = 0;
            try
            {
                retvalue = (from a in oTaxRepo.GetModel() where a.IsDefault==true select a.ValuePercent).SingleOrDefault();
            }
            catch (Exception ex)
            {
                retvalue = 0;
            }
            return retvalue;
        }


        public int GetTaxPercent(int value)
        {
            int retvalue = 0;
            try
            {
                retvalue = (from a in oTaxRepo.GetModel() where a.SlabStart >= value && a.SlabEnd <= value select a.ValuePercent).SingleOrDefault();
            }catch(Exception ex)
            {
                retvalue = 0;
            }
            return retvalue;
        }
    }
}
