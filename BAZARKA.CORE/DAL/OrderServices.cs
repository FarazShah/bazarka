﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BAZARKA.CORE.Models;
using BAZARKA.CORE.ViewModels;
namespace BAZARKA.CORE.DAL
{
    public class OrderServices
    {
        AllRepository<Order> OrderRepo = null;
        AllRepository<OrderDetail> OrderDetailRepo = null;
        public static OrderServices Instance
        {
            get
            {
                if (instance == null) instance = new OrderServices();
                return instance;
            }
        }

        private static OrderServices instance { get; set; }

        private OrderServices()
        {
            OrderRepo = new AllRepository<Order>();
            OrderDetailRepo = new AllRepository<OrderDetail>();
        }

        public int GenerateOrder(VM_PlaceOrder orderDetails, List<VM_CartProducts> oCartProducts, CustomerProfile customer, out string description)
        {
            description = string.Empty;
            int OrderID = 0;
            try
            {
                List<Product> oProducts = null;
                List<string> SKUs = oCartProducts.Select(x => x.sku).ToList();
                if (SKUs != null && SKUs.Count > 0)
                {
                    oProducts = ProductServices.Instance.GetProducts(SKUs);
                }
                int ordercount = 0;
                if (oProducts != null && oProducts.Count > 0 && orderDetails != null)
                {
                    var newOrder = new Order
                    {
                        Active = true,
                        CreateDate = DateTime.Now,
                        CreatedBy = "Customer",
                        CustomerID = 0,
                        ErrDesc = string.Empty,
                        ErrMsg = string.Empty,
                        FullFiled = false,
                        OrderDate = DateTime.Now.Date,
                        PaymentCode = "CAD",
                        PaymentDate = null,
                        PaymentID = 2,
                        ProcessID = 2,
                        ProcessCode = "INI",
                        RequiredDate = DateTime.Now.Date.AddDays(5),
                        SalesTax = VM_OrderSummary.TaxPercentApplied,
                        ShipDate = DateTime.Now.Date.AddDays(5),
                        ShipperiD = 1,
                        TransactStatus = null,
                        UpdateBy = null,
                        UpdateDate = null,
                        Address1 = orderDetails.Address,
                        Address2 = string.Empty,
                        Address3 = string.Empty,
                        Address4 = string.Empty,
                        City = orderDetails.Address,
                        Country = "PAKISTAN",
                        ContactNumber = orderDetails.Phone,
                        OrdersDetails = new List<OrderDetail>()
                    };
                    ordercount = 1;
                    foreach (var obj in oProducts)
                    {
                        newOrder.OrdersDetails.Add(new OrderDetail
                        {
                            Color = string.Empty,
                            Discount = 0,
                            FulFilled = false,
                            Price = obj.UnitPrice,
                            ProductSKU = obj.SKU,
                            ProductID = obj.ProductID,
                            PromoCode = null,
                            PromoID = 0,
                            Quantity = oCartProducts.Where(x => x.sku == obj.SKU).Select(x => x.quantity).SingleOrDefault(),
                            Rejected = false,
                            RejectionDetail = string.Empty,
                            SalesTax = VM_OrderSummary.Tax,
                            ShipDate = newOrder.ShipDate,
                            ShipperID = 1,
                            ShippingCharges = Convert.ToDouble(VM_OrderSummary.Shipping),
                            Size = string.Empty,
                            Total = obj.UnitPrice,
                            DueDate = DateTime.Now.AddDays(5)
                        });
                        description += obj.ProductName + " " + obj.SKU + " " + oCartProducts.Where(x => x.sku == obj.SKU).Select(x => x.quantity).SingleOrDefault() + " " + obj.UnitPrice + "-";

                        ++ordercount;
                    }
                    OrderRepo.InsertModel(newOrder);
                    int OrderInsertCount = OrderRepo.Save();
                    if (OrderInsertCount == ordercount)
                    {
                        orderDetails.IsOrderPosted = true;
                        OrderID = GetLastOrderID();
                    }
                }
            }
            catch (Exception ex)
            {
                OrderID = 0;
            }
            return OrderID;
        }

        public int GetLastOrderID()
        {
            int retvalue = 0;
            retvalue = (from t in OrderRepo.GetModel() where t.Active = true orderby t.OrderID descending select t.OrderID).Take(1).FirstOrDefault();
            return retvalue;
        }

        public List<Order> GetAllOrders()
        {
            List<Order> orders = new List<Order>();
            try
            {
                orders = (from a in OrderRepo.GetModel() where a.Active = true orderby a.CreateDate descending select a).ToList();
            }
            catch (Exception ex)
            {

            }
            return orders;
        }

        public VM_OrderandDetails GetOrderAndDetailByOrderId(int id)
        {
            VM_OrderandDetails oOrders = null;
            try
            {
                oOrders = new VM_OrderandDetails();
                oOrders.Order = (from a in OrderRepo.GetModel() where a.OrderID == id select a).FirstOrDefault();
                oOrders.OrderDetails = (from b in OrderDetailRepo.GetModel() where b.OrderID == id select b).ToList();
            }
            catch (Exception ex)
            {
                oOrders = null;
            }
            return oOrders;
        }
    }
}
