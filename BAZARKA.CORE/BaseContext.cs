﻿using BAZARKA.CORE.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace BAZARKA.CORE.Identity
{
    public class BaseContext : IdentityDbContext<AppUser>
    {
        public BaseContext() : base("name=BaseContext")
        {

        }
        public DbSet<BannerInformation> BannerInformations { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<CustomerProfile> CustomerProfiles { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<ProcessFlow> ProcessFlows { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductDetail> ProductDetails { get; set; }
        public DbSet<Promotions> Promotions { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<SearchHistory> SearchHistorys { get; set; }
        public DbSet<Subscribers> SubsScribers { get; set; }
        public DbSet<Vendor> Vendors { get; set; }
        public DbSet<CustomerInvoice> CustomerInvoices { get; set; }
        public DbSet<Taxation> Taxations { get; set; }
        public DbSet<Shipping> Shipping { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public System.Data.Entity.DbSet<BAZARKA.CORE.ViewModels.VM_OrderandDetails> VM_OrderandDetails { get; set; }
    }
}