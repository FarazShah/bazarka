﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BAZARKA.CORE.Models
{
    public class Category
    {
        [Key]
        public int CatID { get; set; }
        [StringLength(50)]
        [Index(IsUnique = true)]
        public string CatCode { get; set; }
        public string Name { get; set; }
        public int ParentID { get; set; }
        
    }
}
