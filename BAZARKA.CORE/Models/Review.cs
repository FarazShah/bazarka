﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace BAZARKA.CORE.Models
{       
    public class Review
    {
        [Key]
        public int ReviewID { get; set; }
        public string ProductSKU { get; set; }
        public string Title { get; set; }
        public string Desciption { get; set; }
        public int Star { get; set; }
        public int HelpFull { get; set; }
        public int NotHelpFull { get; set; }
        public DateTime CreateDate { get; set; }
        public String UpdateDate { get; set; }
        public string UpdateBy { get; set; }

        public string CustomerID { get; set; }

        public int ProductID { get; set; }
    }
}
