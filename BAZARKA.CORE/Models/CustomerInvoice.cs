﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAZARKA.CORE.Models
{
    public class CustomerInvoice
    {
        [Key]
        public int InvoiceNumber { get; set; }
        public int CustomerID { get; set; }
        public int OrderID { get; set; }
        public string Email { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime DueDate { get; set; }
        public string InvoiceTo { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Description { get; set; }
        public string ShippingCode { get; set; }
        public int TaxPercent { get; set; }
        public double Total { get; set; }
        public string Note { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
    }
}
