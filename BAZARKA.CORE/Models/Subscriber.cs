﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BAZARKA.CORE.Models
{
    public class Subscribers 
    {
        [Key]
        public int SubID { get; set; }
        [StringLength(100)]
        [Index(IsUnique = true)]
        public string Email { get; set; }
        public bool Active { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
