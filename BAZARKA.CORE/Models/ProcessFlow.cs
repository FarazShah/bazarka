﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BAZARKA.CORE.Models
{
    public class ProcessFlow
    {
        [Key]
        public int ProcessID { get; set; }
        [StringLength(50)]
        [Index(IsUnique = true)]
        public string code { get; set; }
        public string Name { get; set; }
    }
}
