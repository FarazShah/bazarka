﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BAZARKA.CORE.Models
{
    public class Vendor
    {
        [Key]
        public int VendorID { get; set; }
        [StringLength(50)]
        [Index(IsUnique = true)]
        public string VendorCode { get; set; }
        public string CompanyName { get; set; }
        public string ContactFName { get; set; }
        public string ContactLName { get; set; }
        public string ContactTitle { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string PaymentMethod { get; set; }
        public string DiscountType { get; set; }
        public Double DiscountRate { get; set; }
        public string TypeGoods { get; set; } //Description of types of goods available from the Supplier.
        public bool DiscountAvailable { get; set; }
        public int CurrentOrder { get; set; }
        public string ProdInfoPageURL { get; set; }
        public string Logo { get; set; }
        public float VendorRanking { get; set; }
        public string Note { get; set; }
        public bool Active { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdateBy { get; set; }
        
    }
}
