﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace BAZARKA.CORE.Models
{
    public class SearchHistory
    {
        [Key]
        public int SearchID { get; set; }
        public string Text { get; set; }
        public DateTime Date { get; set; }
        public int CustomerID {get;set;}
    }
}
