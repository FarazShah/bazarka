﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace BAZARKA.CORE.Models
{
    public class OrderDetail
    {
        [Key]
        public int OrderDetailID { get; set; }
        public string ProductSKU { get; set; }
        public Double Price { get; set; }
        public int Quantity { get; set; }
        public double Discount { get; set; }
        public double Total { get; set; }
        public string Size { get; set; }
        public string Color { get; set; }
        public bool FulFilled { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime ShipDate { get; set; }
        public int ShipperID { get; set; }
        public double ShippingCharges { get; set; }
        public double SalesTax { get; set; }
        public bool Rejected { get; set; }
        public string RejectionDetail { get; set; }

        public int OrderID { get; set; }
        //public virtual Order Order { get; set; }

        public int ProductID { get; set; }

        public int PromoID { get; set; }
        public string PromoCode { get; set; }
    }
}
