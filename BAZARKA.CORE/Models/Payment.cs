﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BAZARKA.CORE.Models
{
    public class Payment
    {
        [Key]
        public int PaymentID { get; set; }
        [StringLength(50)]
        [Index(IsUnique =true)]
        public string PaymentCode { get; set; }
        public string PaymentName { get; set; }
    }
}
