﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BAZARKA.CORE.Models
{
    public class Product
    {
        [Key]
        public int ProductID { get; set; }
        [StringLength(50)]
        [Index(IsUnique = true)]
        public string SKU { get; set; }
        public int VendorProductID { get; set; }
        public string ProductName { get; set; }
        public string ProductBrand { get; set; }
        public string ProductThumbnail { get;   set; }
        public int QTYPerUnit { get; set; }
        public int UnitSize { get; set; } 
        public int UnitPrice { get; set; }
        public double MSRP { get; set; }
        public string AvailableSize { get; set; }
        
        public int SizeID { get; set; } //needs to be created
        public int ColorID { get; set; } //needs to be created

        public double Discount { get; set; }
        public double UnitWeight { get; set; }
        public int UnitsInStock { get; set; }
        public int UnitOnOrder { get; set; }
        public int ReOrderLevel { get; set; }
        public bool Availability { get; set; }
        public bool DiscountAvailable { get; set; }

        public int weightage { get; set; }
        public string SearchWeight { get; set; }

        public string Note { get; set; }
        public bool Active { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public int ProductDetailID { get; set; }
        public virtual ProductDetail ProductDetail { get; set; }

        public int VendorID { get; set; }
        public string VendorCode { get; set; }

        public int CatID { get; set; }
        public string CategoryCode { get; set; }
        

    }
}
