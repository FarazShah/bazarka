﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BAZARKA.CORE.Models
{
    public class ProductDetail
    {
        [Key, ForeignKey("Product")]
        public int ProductDetailID { get; set; }
        public string ShortDesc { get; set; }
        public string LongDesc { get; set; }
        public string Specification { get; set; }
        public string WhatInTheBox { get; set; }
        public string RefundPolicy { get; set; }
        public string ProductWarrantyShort { get; set; }
        public string ProductWarrantyLong { get; set; }
        public string image1 { get; set; }
        public string image2 { get; set; }
        public string image3 { get; set; }
        public string image4 { get; set; }
        public string FreeShipping { get; set; }
        public string VideoURL { get; set; }
        public string MetaTitle { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
        public int WeightGrams { get; set; }
        public string ProductKeywords { get; set; }
        public string DeliveryTime { get; set; }
        public string YoutubeLink { get; set; }
        public string SKUCity { get; set; }
        public string UPC { get; set; }
        public string HelpText { get; set; }
        public virtual Product Product{ get; set; }
    }
}
