﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAZARKA.CORE.Models
{
    public class Taxation
    {
        public int ID { get; set; }
        [StringLength(50)]
        [Index(IsUnique = true)]
        public string TaxCode { get; set; }
        public string TaxDeciption { get; set; }
        public int SlabStart { get; set; }
        public int SlabEnd { get; set; }
        public int ValuePercent { get; set; }
        public bool IsDefault { get; set; }
        public bool Active { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}
