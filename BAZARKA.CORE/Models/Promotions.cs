﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BAZARKA.CORE.Models
{
    public class Promotions
    {
        [Key]
        public int PromoID { get; set; }
        [StringLength(50)]
        [Index(IsUnique = true)]
        public string PromoCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double Price { get; set; }
        public double MSRP { get; set; }
        public int Quantity { get; set; }
        public string PromoPaymentMethod { get; set; }
        public int PromoCyclePerCustomer { get; set; }
        public string Type { get; set; }
        public int BIN { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        
    }
}
