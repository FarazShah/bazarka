﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace BAZARKA.CORE.Models
{
    public class Order
    {
        [Key]
        public int OrderID { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime RequiredDate { get; set; }
        public DateTime ShipDate { get; set; }
        public string ContactNumber { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public int ShipperiD { get; set; }
        public double SalesTax { get; set; }
        public string TransactStatus { get; set; }
        public string ErrMsg { get; set; }
        public string ErrDesc { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? PaymentDate { get; set; }
        public bool FullFiled { get; set; }
        public bool Active { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdateBy { get; set; }

        public List<OrderDetail> OrdersDetails { get; set; }

        public int CustomerID { get; set; }

        public int PaymentID { get; set; }
        public string PaymentCode { get; set; }

        public int ProcessID { get; set; }
        public string ProcessCode { get; set; }
    }
}
