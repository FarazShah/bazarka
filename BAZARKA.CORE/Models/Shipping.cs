﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAZARKA.CORE.Models
{
    public class Shipping
    {
        [Key]
        public int ID { get; set; }
        [StringLength(50)]
        [Index(IsUnique = true)]
        public string Code { get; set; }
        public string Description { get; set; }
        public int Cost { get; set; }
        public bool IsDefault { get; set; }
        public bool Active { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string CreatedBy { get; set; }
        public String UpdatedBy { get; set; }
    }
}
