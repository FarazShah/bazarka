﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.WebPages;

namespace BAZARKA.WEB
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            InitializeDisplayModeProviders();
        }

        protected void InitializdeBundles()
        {
            var PhoneScripts = new ScriptBundle("~/Context/Mobile/");
            var PhoneStyles = new ScriptBundle("~/Context/Mobile/");

            BundleTable.Bundles.IgnoreList.Clear();
            BundleTable.Bundles.Add(PhoneScripts);
            BundleTable.Bundles.Add(PhoneStyles);

        }

        protected void InitializeDisplayModeProviders()
        {
            var phone = new DefaultDisplayMode("Phone")
            {
                ContextCondition = ctx=> ctx.GetOverriddenUserAgent() != null && ctx.GetOverriddenUserAgent().Contains("iphone")
            };

            var tablet = new DefaultDisplayMode("Tablet")
            {
                ContextCondition = ctx => ctx.GetOverriddenUserAgent() != null && ctx.GetOverriddenUserAgent().Contains("ipad")
            };

            DisplayModeProvider.Instance.Modes.Insert(0, phone);
            DisplayModeProvider.Instance.Modes.Insert(1, tablet);

        }
    }
}
