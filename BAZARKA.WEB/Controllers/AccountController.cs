﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Owin;
using System.Web.Mvc;
using BAZARKA.CORE.Identity;
using BAZARKA.CORE.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using BAZARKA.CORE.DAL;
using System.Threading.Tasks;
using BAZARKA.CORE;

namespace BAZARKA.WEB.Controllers
{
    public class AccountController : Controller
    {
        private _IRepository<CustomerProfile> InterfaceObj;
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<JsonResult> RegisterAsync(string Email, string Pwd, string ConfirmPwd, string CellNum, DateTime DOB )
        {
            string responseText = string.Empty;
            bool success = false;
            if (ModelState.IsValid)
            {
                var userManager = HttpContext.GetOwinContext().GetUserManager<AppUserManager>();
                var AuthManager = HttpContext.GetOwinContext().Authentication;

                var user = new AppUser { UserName = "Faraz5", Email = Email, DateOfBirth= DOB };
                var result = await userManager.CreateAsync(user, Pwd);

                if (result.Succeeded && user != null)
                {
                    using (BaseContext OContext = new BaseContext())
                    {
                        try
                        {
                            AppUser ApplicationUser = userManager.Find(user.UserName, user.PasswordHash);
                          
                            var ident = userManager.CreateIdentity(ApplicationUser, DefaultAuthenticationTypes.ApplicationCookie);
                            AuthManager.SignIn(new Microsoft.Owin.Security.AuthenticationProperties { IsPersistent = false }, ident);
                            success = true;
                            responseText = "User Registered Successfully";
                        }
                        catch (Exception ex)
                        {
                            success = false;
                            responseText = ex.Message.ToString();
                        }
                    }
                }
                else
                {
                    success = false;
                    responseText = result.Errors.FirstOrDefault().ToString();
                }
            }
            return Json(new { success = success, responseText = responseText.ToString() });
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(String Email, String Password, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var userManager = HttpContext.GetOwinContext().GetUserManager<AppUserManager>();
                var AuthManager = HttpContext.GetOwinContext().Authentication;
                var user = await userManager.FindAsync(Email, Password);
                if (user != null)
                {
                    AuthManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                    var identity = await userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
                    AuthManager.SignIn(new Microsoft.Owin.Security.AuthenticationProperties { IsPersistent = false }, identity);
                }
                else
                {
                    ModelState.AddModelError("", "Invalid username or password.");
                }
            }
            return View(returnUrl);
        }



    }
}