﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BAZARKA.CORE.DAL;
using BAZARKA.CORE.ViewModels;
using System.Web.Script.Serialization;

namespace BAZARKA.WEB.Controllers
{
    public class ProductController : Controller
    {
        public ActionResult Index()
        {
            //List<VM_Product> Products = LoadData(1, 50);
            return View();
        }

        public JsonResult LoadData(int pagenumber, int pagesize)
        {
            List<VM_Product> Products = null;
            JavaScriptSerializer js = new JavaScriptSerializer();
            VM_Product obj = new VM_Product();
            try
            {
               Products = ProductServices.Instance.GetProducts(pagenumber, pagesize);
            }catch(Exception ex)
            {
                Products = null;
            }
            //return Json(js.Serialize(Products));
            return Json(Products);
        }
    }
}