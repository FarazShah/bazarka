﻿using System.Web.Mvc;
using BAZARKA.CORE.DAL;
//using BAZARKA.CORE.Models;
using System.Collections.Generic;
using BAZARKA.CORE;
using BAZARKA.CORE.ViewModels;
using BAZARKA.CORE.Models;
using System;

namespace BAZARKA.WEB.Controllers
{
    public class HomeController : Controller
    {
        HomeServices oHomeServices;
        public HomeController()
        {
            oHomeServices = new HomeServices();
        }

        public ActionResult Index()
        {
            List<Product> JustArrived = oHomeServices.GetJustArrived();
            ViewBag.JustArrived = JustArrived;
            List<Product> EditorPick = oHomeServices.GetEditorsPick();
            ViewBag.EditorPick = EditorPick;
            List<VM_Product> BestSeller = oHomeServices.GetBestSellers();
            ViewBag.BestSeller = BestSeller;
            return View();
        }

        public ActionResult RecommendedForYou()
        {
            try
            {

            }catch(Exception ex)
            {

            }
            return PartialView();
        }
        
    }
}