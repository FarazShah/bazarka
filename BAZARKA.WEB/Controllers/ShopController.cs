﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BAZARKA.CORE.DAL;
using BAZARKA.CORE.Models;
using BAZARKA.CORE.ViewModels;

namespace BAZARKA.WEB.Controllers
{
    public class ShopController : Controller
    {
        ShippingServices oShippingService = null;
        TaxServices oTaxService = null;
        AllRepository<CustomerInvoice> oCustInvoiceRepo = null;

        public ActionResult Checkout()
        {
            return View();
        }

        public ActionResult Basket()
        {
            List<Product> model = null;
            List<VM_CartProducts> ProductSKUs = GetCartProducts();
            if (ProductSKUs != null)
            {
                List<string> SKUs = ProductSKUs.Select(x => x.sku).ToList();
                model = ProductServices.Instance.GetProducts(SKUs);
            }
            return View(model);
        }

        public ActionResult MiniBasket()
        {
            List<Product> model = null;
            List<VM_CartProducts> CartProducts = GetCartProducts();
            if (CartProducts != null && CartProducts.Count > 0)
            {
                var productSKUs = CartProducts.Select(x => x.sku).ToList();
                model = ProductServices.Instance.GetProducts(productSKUs);
                for (int i = 0; i < model.Count; i++)
                {
                    model[i].UnitsInStock = CartProducts.Where(x => x.sku == model[i].SKU).Select(x => x.quantity).FirstOrDefault();
                }
            }
            return PartialView(model);
        }

        #region Cart
        public bool SetCartProducts(List<VM_CartProducts> CartProducts)
        {
            bool retval = false;
            try
            {
                if (CartProducts != null && CartProducts.Count() > 0)
                {
                    HttpCookie cookie = Request.Cookies["CartProducts"];
                    cookie.Value = string.Empty;
                    for (int i = 0; i < CartProducts.Count(); i++)
                    {
                        if (i == 0)
                        {
                            cookie.Value += CartProducts[i].sku + "|" + CartProducts[i].quantity.ToString();
                        }
                        else
                        {
                            cookie.Value += "-";
                            cookie.Value += CartProducts[i].sku + "|" + CartProducts[i].quantity.ToString();
                        }
                    }
                    Response.SetCookie(cookie);
                    retval = true;
                }
                else
                {
                    HttpCookie cookie = Request.Cookies["CartProducts"];
                    cookie.Value = string.Empty;
                    Response.SetCookie(cookie);
                    retval = true;
                }
            }
            catch (Exception ex)
            {
                retval = false;
            }
            return retval;
        }

        public List<VM_CartProducts> GetCartProducts()
        {
            List<VM_CartProducts> oCartProducts = null;
            List<string> oProd = new List<string>();
            VM_CartProducts oCartProduct = null;
            var CartProductsCookie = Request.Cookies["CartProducts"];
            if (CartProductsCookie != null && CartProductsCookie.Value.Count() > 0)
            {
                oCartProducts = new List<VM_CartProducts>();
                oProd = CartProductsCookie.Value.Split('-').Select(x => x.ToString()).ToList();
                foreach (var obj in oProd)
                {
                    oCartProduct = new VM_CartProducts();
                    int StrtPos = obj.IndexOf("|") + 1;
                    string Qty = obj.Substring(StrtPos);
                    string SKU = obj.Substring(0, StrtPos - 1);
                    oCartProduct.quantity = Convert.ToInt32(Qty);
                    oCartProduct.sku = SKU;
                    oCartProducts.Add(oCartProduct);
                }
            }
            return oCartProducts;
        }

        public bool EmptyCartProducts()
        {
            bool retval = false;
            try
            {
                var CartProductsCookie = Request.Cookies["CartProducts"];
                if (CartProductsCookie != null && CartProductsCookie.Value.Count() > 0)
                {
                    CartProductsCookie.Value = null;
                    Response.SetCookie(CartProductsCookie);
                    retval = true;
                }
                else
                {
                    retval = true;
                }
            }
            catch (Exception ex)
            {
                retval = false;
            }
            return retval;
        }

        [HttpPost]
        public JsonResult AddProduct(string productSku)
        {
            string retMessage = string.Empty;
            bool success = false;
            try
            {
                List<VM_CartProducts> ExistingCartProducts = GetCartProducts();
                if (ExistingCartProducts != null && ExistingCartProducts.Count() > 0)
                {
                    if (ExistingCartProducts.Exists(x => x.sku.ToUpper() == productSku.ToUpper()))
                    {
                        retMessage = "Product Already in Cart!";
                    }
                    else
                    {
                        VM_CartProducts NewProduct = new VM_CartProducts();
                        NewProduct.sku = productSku;
                        NewProduct.quantity = 1;
                        ExistingCartProducts.Add(NewProduct);
                        success = SetCartProducts(ExistingCartProducts);
                        if (success)
                        {
                            retMessage = "Product Added to cart!";
                        }
                        else
                        {
                            retMessage = "Error occured!";
                        }
                    }
                }
                else
                {

                    HttpCookie cookie = new HttpCookie("CartProducts");
                    cookie.Name = "CartProducts";
                    cookie.Path = "/";
                    cookie.Value += productSku + "|1";
                    cookie.Expires = DateTime.Now.AddDays(30);
                    Response.Cookies.Add(cookie);
                    success = true;
                }
            }
            catch (Exception ex)
            {
                success = false;
                retMessage = "Error occured";
            }
            return Json(new { success = success, responseText = retMessage }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RemoveProduct(string productSku)
        {
            string retMessage = string.Empty;
            bool success = false;
            try
            {
                List<VM_CartProducts> CartProducts = GetCartProducts();
                if (CartProducts != null && CartProducts.Count > 0)
                {
                    if (CartProducts.Exists(x => x.sku == productSku))
                    {
                        VM_CartProducts obj = CartProducts.Where(x => x.sku == productSku).SingleOrDefault();
                        if (obj != null)
                        {
                            CartProducts.Remove(obj);
                        }
                        success = SetCartProducts(CartProducts);
                        if (success)
                        {
                            retMessage = "Product Removed";
                        }
                        else
                        {
                            retMessage = "Error occured";
                        }
                    }
                }
                else
                {
                    retMessage = "Cart is empty!";
                    success = false;
                }
            }
            catch (Exception ex)
            {
                success = false;
                retMessage = "Error occured";
            }
            return Json(new { success = success, responseText = retMessage }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateProduct(string productSku, int qty)
        {
            string retMessage = string.Empty;
            bool success = false;
            try
            {
                List<VM_CartProducts> CartProducts = GetCartProducts();
                VM_CartProducts ProductToUpdate = CartProducts.Where(x => x.sku == productSku).FirstOrDefault();
                if (CartProducts != null && CartProducts.Count > 0 && ProductToUpdate != null)
                {
                    int Index = CartProducts.IndexOf(ProductToUpdate);
                    CartProducts[Index].quantity = qty;
                    success = SetCartProducts(CartProducts);
                }
            }
            catch (Exception ex)
            {
                success = false;
            }
            return Json(new { success = success, responseText = retMessage }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        private bool EmptyOrderSummary()
        {
            bool retval = false;
            try
            {
                VM_OrderSummary.EstimatedTotal = 0;
                VM_OrderSummary.MerchandiseSubTotal = 0;
                VM_OrderSummary.Shipping = 0;
                VM_OrderSummary.Tax = 0;
                VM_OrderSummary.TaxPercentApplied = 0;
                retval = true;
            }
            catch(Exception ex)
            {
                retval = false;
            }
            return retval;
        }

        [HttpGet]
        public ActionResult PlaceOrder()
        {
            return PartialView(new VM_PlaceOrder());
        }

        [HttpPost]
        public ActionResult PlaceOrder(VM_PlaceOrder oPlaceOrder)
        {
            int orderID = 0;
            if (ModelState.IsValid)
            {
                bool IsSuccess = false;
                try
                {
                    oCustInvoiceRepo = new AllRepository<CORE.Models.CustomerInvoice>();
                    List<VM_CartProducts> CartProducts = GetCartProducts();
                    if (CartProducts != null && oPlaceOrder != null)
                    {
                        string description;
                        orderID = OrderServices.Instance.GenerateOrder(oPlaceOrder, CartProducts, null, out description);
                        ViewBag.OrderID = orderID;
                        if (orderID > 0)
                        {
                            oPlaceOrder.IsOrderPosted = true;
                            CustomerInvoice NewInvoice = new CustomerInvoice()
                            {
                                Address = oPlaceOrder.Address,
                                City = oPlaceOrder.City,
                                CreatedBy = "System",
                                CreatedDate = DateTime.Now,
                                CustomerID = 0,
                                Description = description,
                                DueDate = DateTime.Now.AddDays(5),
                                Email = oPlaceOrder.Email,
                                InvoiceDate = DateTime.Now,
                                InvoiceTo = oPlaceOrder.FirstName + " " + oPlaceOrder.LastName,
                                Note = "Note",
                                ShippingCode = oPlaceOrder.ShippingCode,
                                TaxPercent = VM_OrderSummary.TaxPercentApplied,
                                Total = VM_OrderSummary.EstimatedTotal,
                                UpdateDate = null,
                                UpdatedBy = null,
                                OrderID = orderID
                            };
                            oCustInvoiceRepo.InsertModel(NewInvoice);
                            int retval = oCustInvoiceRepo.Save();
                            if (retval == 1)
                            {
                                if (EmptyCartProducts() && EmptyOrderSummary())
                                {
                                    return RedirectToAction("CustomerInvoice", "Shop", new { Address = NewInvoice.Address, City=NewInvoice.City, Description= NewInvoice.Description, InvoiceTo=NewInvoice.InvoiceTo, InvoiceDate = NewInvoice.InvoiceDate, DueDate= NewInvoice.DueDate});
                                }
                                else
                                {
                                    IsSuccess = false;
                                    throw new Exception();
                                }
                            }
                        }
                        else
                        {
                            IsSuccess = false;
                            throw new Exception();
                        }
                    }
                    else
                    {
                        IsSuccess = false;
                        throw new Exception();
                    }
                }
                catch (Exception ex)
                {
                    IsSuccess = false;
                }
            }
            return PartialView();
        }
        
        public ActionResult CustomerInvoice(CustomerInvoice InvoiceDetail)
        {
            return PartialView();
        }

        public ActionResult OrderSummary()
        {
            ViewBag.Total = 0;
            ViewBag.Shipping = 0;
            ViewBag.Tax = 0;
            double ToTal = 0;

            List<VM_CartProducts> oProductList = GetCartProducts();
            if (oProductList != null && oProductList.Count > 0)
            {
                oShippingService = new ShippingServices();
                oTaxService = new TaxServices();
                foreach (var obj in oProductList)
                {
                    ToTal += ProductServices.Instance.GetProductUnitPrice(obj.sku) * obj.quantity;
                }
                VM_OrderSummary.MerchandiseSubTotal = ToTal;
                VM_OrderSummary.Shipping = oShippingService.GetDefaultShippingCost();
                VM_OrderSummary.Tax = 0.00;
                VM_OrderSummary.TaxPercentApplied = oTaxService.GetDefaulTaxPercent();
                VM_OrderSummary.EstimatedTotal = ToTal + VM_OrderSummary.Shipping + VM_OrderSummary.Tax;
            }
            return PartialView();
        }
    }
}