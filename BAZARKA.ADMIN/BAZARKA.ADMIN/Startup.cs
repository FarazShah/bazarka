﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BAZARKA.ADMIN.Startup))]
namespace BAZARKA.ADMIN
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
