﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BAZARKA.CORE.DAL;
using BAZARKA.CORE.ViewModels;
using BAZARKA.CORE.Models;
using System.Web.Script.Serialization;

namespace BAZARKA.ADMIN.Controllers
{
    public class OrderController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetData()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

            var process = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
            var contactNumber = Request.Form.GetValues("columns[3][search][value]").FirstOrDefault();
            var address = Request.Form.GetValues("columns[4][search][value]").FirstOrDefault();
            var city = Request.Form.GetValues("columns[5][search][value]").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;

            var orders = OrderServices.Instance.GetAllOrders();

            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
            {
                //orders = orders.OrderBy(sortColumn + " " + sortColumnDir);
            }

            if (!string.IsNullOrEmpty(searchValue))
            {
                orders = orders.Where(m => m.ProcessCode.Contains(searchValue) || m.ContactNumber.Contains(searchValue) || m.Address1.Contains(searchValue) || m.City.Contains(searchValue)).ToList();
            }

            recordsTotal = orders.Count();

            var data = orders.Skip(skip).Take(pageSize).ToList();

            //JavaScriptSerializer js = new JavaScriptSerializer();
            return Json(new { draw = draw, recordsFiltered =recordsTotal, recordsTotal = recordsTotal, data=data});
        }
        
        [HttpGet]
        public ActionResult Detail(int ID)
        {
            VM_OrderandDetails orders = OrderServices.Instance.GetOrderAndDetailByOrderId(ID);
            return View(orders);
        }

        [HttpGet]
        public ActionResult Update(int ID)
        {
            VM_OrderandDetails orders = OrderServices.Instance.GetOrderAndDetailByOrderId(ID);
            return View(orders);
        }

        [HttpGet]
        public ActionResult UpdateOrderItem(int ID)
        {
            return View();
        }
    }
}